const path = require("path");
const autoprefixer = require("autoprefixer");
const webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js",
    chunkFilename: "[id].js",
  },
  // context: path.join(__dirname, 'your-app'),
  resolve: {
    extensions: [".js", ".jsx"],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        loader: "url-loader?limit=10000&name=img/[name].[ext]",
      },
    ],
  },
  devServer: {
    compress: true,
    // contentBase: path.resolve(__dirname, '.'),
    port: process.env["frontendPort"] || 1234,
    host: "0.0.0.0",
    disableHostCheck: true,
    overlay: true,
    hot: true,
    inline: true,
    watchOptions: {
      poll: true,
    },
    public: process.env["webpackPublicDomain"],
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      projectUUID: "playcent-project-uuid",
      dbDomain: "http://playcent.com",
      userid: "username",
    }),
    new CopyWebpackPlugin({
      patterns: [{ from: "public/assets", to: "assets" }],
    }),
    new HtmlWebpackPlugin({
      template: __dirname + "/public/index.html",
      filename: "index.html",
      inject: "body",
    }),
    new MiniCssExtractPlugin(),
  ],
};
